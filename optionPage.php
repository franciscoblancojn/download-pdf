<?php
add_action( 'admin_menu', 'DPDF_option_page' );
 
function DPDF_option_page() {
	add_menu_page(
		'Download PDF invoices', // page <title>Title</title>
		'Download PDF invoices', // menu link text
		'manage_options', // capability to access the page
		'DPDF-slug', // page URL slug
		'DPDF_option_page_function', // callback function /w content
		'dashicons-pdf', // menu icon
		5 // priority
	);
 
}
add_action( 'admin_init',  'misha_register_setting' );
 
function DPDF_option_page_function(){
    $args = array(
        'limit'     => 99999999999999999999999999999,
        'status'    => array('wc-processing', 'wc-completed'),
        'return'    => 'ids',
    );
    if(isset($_GET) && isset($_GET['filter']) && $_GET['filter']=="true"){
        $args['date_created'] = $_GET['dateMin'] .'...'. $_GET['dateMax'] ;
    }

    $orders = wc_get_orders( $args );
    $countOrder = count($orders);
    $orders = json_encode($orders);
    ?>
    <div class="wrap">
	    <h1>Download PDF invoices</h1>
        <div>
            <h3>Filter</h3>
            <form method="get">
                <input type="hidden" name="filter" value="true">
                <input type="hidden" name="page" value="DPDF-slug">
                <label>
                    Date Min
                    <input type="date" name="dateMin" id="dateMin" value="<?=$_GET['dateMin']?>">
                </label>
                <label>
                    Date Max
                    <input type="date" name="dateMax" id="dateMax" value="<?=$_GET['dateMax']?>">
                </label>
                <button class="button">Filter</button>
            </form>
        </div>
        <div id="textMeterDownloadPDF"></div>
        <meter id="meterDownloadPDF" value="0" min="0" max="<?=$countOrder?>" hidden></meter>
        <br>
        <button 
        id="download_pdf"
        class="button button-primary"
        >
            Download PDF invoices
        </button>
        <style>
            #meterDownloadPDF{
                width: 500px;
                height: 40px;
            }
        </style>
        <script>
            const orders = <?=$orders?>;
            var cDownload = 0; 
            var btnDPDF = document.getElementById('download_pdf')
            var meterDPDF = document.getElementById('meterDownloadPDF')
            var textMeterDownloadPDF = document.getElementById('textMeterDownloadPDF')

            btnDPDF.onclick = () => {
                meterDPDF.hidden = false
                cDownload = 0
                meterDPDF.value = cDownload
                downloadPdfOrder()
            }

            const getOrderDocument = ( order_id , respondeFunction = (e) => console.log(e) ) => {
                fetch(`/wp-admin/post.php?post=${order_id}&action=edit`)
                .then(response => response.text())
                .then(data => { 
                    var parser = new DOMParser();
                    var doc = parser.parseFromString(data, 'text/html');
                    respondeFunction(doc)
                })
            }
            const getPdfByUrl = ( order_id , url , respondeFunction = (e) => console.log(e) ) => {
                fetch(url)
                .then(response => response.blob())
                .then(data => {
                    var url = window.URL.createObjectURL(data);
                    var a = document.createElement('a');
                    a.href = url;
                    a.download = `PDF_Invoice_${order_id}.pdf`;
                    document.body.appendChild(a); 
                    a.click();    
                    a.remove(); 
                    respondeFunction( order_id )
                })
            }
            const downloadPDF = ( order_id , respondeFunction = (e) => console.log(e)) => {
                getOrderDocument(order_id,(e) => {
                    const btn = e.querySelector('[alt="PDF Invoice"]')
                    const urlPDF = btn.href
                    getPdfByUrl( order_id , urlPDF , (order_id) => {
                        respondeFunction(order_id);
                    })
                })
            }
            const downloadPdfOrder = () => {
                if( cDownload >= orders.length)return;
                downloadPDF(orders[cDownload],(e)=>{
                    cDownload++
                    meterDPDF.value = cDownload
                    textMeterDownloadPDF.innerHTML = `${cDownload}/${orders.length}----${(cDownload/orders.length)*100}%`
                    downloadPdfOrder(cDownload)
                })
            }

        </script>
    </div>
    <?php
}